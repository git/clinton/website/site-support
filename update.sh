#!/bin/sh

(cd src/unknownlamer.org && git push)

(cd site/unknownlamer.org-repo && git pull)

(cd site-support && clisp -i ./books.lisp -x '(dump-muse-books-file)')

if [ "$1" = "--force" ]; then
    emacsclient --eval '(muse-project-publish "unknownlamer.org-clean" t)'
else
    emacsclient --eval '(muse-project-publish "unknownlamer.org-clean")'
fi

# something is broken in my muse setup but just work around it for now
rm -f site/unknownlamer.org/index.html
emacsclient --eval '(muse-project-publish "unknownlamer.org-clean")'

cd site/unknownlamer.org
git pull ../unknownlamer.org-repo  --no-edit
find . -name \* -print0 | xargs -0 git add
git commit --all -m "html update"
git push /afs/hcoop.net/user/c/cl/clinton/.hcoop-git/website/site/unknownlamer.org.git


(cd /afs/hcoop.net/user/c/cl/clinton/public_html/unknownlamer.org/www/muse && git pull)
